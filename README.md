# linmodel

A shiny application for linear models and nonparametric tests.

**linmodel** provides a shiny application (in French) to facilitate the realisation of some statistical analyses:

* Linear regression
* Analysis of variance
* Kruskal-Wallis test
* Friedman test

## Installation

To install the `linmodel` package from the repository, one can use the [`devtools`](https://github.com/r-lib/devtools) package:

```
install.packages("devtools")
```

Install the [`git2r`](https://github.com/ropensci/git2r) package (to access the repository, if git is not installed on your computer):

```
install.packages("git2r")
```

Install the `linmodel` package:

```
devtools::install_git("https://forgemia.inra.fr/UP_StatInfo/linmodel.git",
                      build_vignettes = TRUE, dependencies = TRUE)
```

## Usage

See vignettes:

```
browseVignettes("linmodel")
```
