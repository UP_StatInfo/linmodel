% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/anovashiny.R
\name{anovashiny}
\alias{anovashiny}
\title{Analyse de la variance}
\usage{
anovashiny()
}
\value{
Retourne le modèle (objet de classe "\code{"lm"}) de façon invisible.
}
\description{
Analyse de la variance
}
\examples{
\dontrun{
anovashiny()
}
}
